using System;
using System.Net.Http;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Google.Protobuf;
using Pentland.LunchAndLearn.Messaging.Client;
using Pentland.LunchAndLearn.Messaging.Messages;
using Pentland.LunchAndLearn.Messaging.Publisher;
using Pentland.LunchAndLearn.Proto;

namespace Pentland.LunchAndLearn.Scraping
{
    /// <summary>
    /// The PageScraper handles processing tweets that contain t.co links
    /// </summary>
    public class PageScraper : IDisposable
    {
        /// <summary>
        /// The topic this scraper is interested in receiving
        /// </summary>
        private const MessageTopic TargetIncomingTopic = MessageTopic.UrlRequest;

        /// <summary>
        /// The topic used when sending a processed message
        /// </summary>
        private const MessageTopic PostProcessOutgoingTopic = MessageTopic.OutputData;

        /// <summary>
        /// Regex pattern for capturing content of page title
        /// </summary>
        private const string Pattern = @"<title>(.*?)</title>";

        /// <summary>
        /// Regex object for matching title
        /// </summary>
        private readonly Regex _titleRegex;

        /// <summary>
        /// Client used for incoming messages
        /// </summary>
        private readonly MessageClient _messageClient;

        /// <summary>
        /// Outgoing message publisher
        /// </summary>
        private readonly MessagePublisher _messagePublisher;

        /// <summary>
        /// Handler for http client
        /// </summary>
        private readonly HttpClientHandler _httpClientHandler;

        /// <summary>
        /// Client used to retrieve linked web pages
        /// </summary>
        private readonly HttpClient _httpClient;

        /// <summary>
        /// The subscription used for incoming messages
        /// </summary>
        private IDisposable _messageSubscription;

        /// <summary>
        /// Create a new PageScraper
        /// </summary>
        /// <param name="messagePublisher">The publisher for outgoing messages</param>
        public PageScraper(MessagePublisher messagePublisher)
        {
            if (messagePublisher == null)
                throw new ArgumentNullException(nameof(messagePublisher));
            
            _titleRegex = new Regex(Pattern, RegexOptions.Compiled);
            _httpClientHandler = new HttpClientHandler();
            _httpClient = new HttpClient(_httpClientHandler);
            _messagePublisher = messagePublisher;
            _messageClient = new MessageClient(TargetIncomingTopic);
        }

        /// <summary>
        /// Begin listening to message client and processing incoming messages
        /// </summary>
        public void BeginProcessingPageRequests()
        {
            //LL6
            // Begin listening to the stream of processing requests and deal with them
            _messageSubscription = _messageClient.MessageObservable
                .Where(msg => msg != null)
                .Select(msg => TweetLinkedPageRequestData.Parser.ParseFrom(msg.Data))
                .Subscribe(RetrieveWebContent);
            _messageClient.RunMessageListener();
        }

        /// <summary>
        /// Retrieve web content for incoming tweets
        /// </summary>
        /// <param name="data">The data</param>
        private async void RetrieveWebContent(TweetLinkedPageRequestData data)
        {
            //LL7
            // If the incoming does not contain a proper link, just send it off to the
            // proper handling function and treat it as one we can't process
            if (string.IsNullOrEmpty(data.Url))
            {
                SendForFailedTitle(data);
                return;
            }
            //LL8
            // Otherwise, process the page link and obtian the title
            var title = await RetrievePageTitle(data.Url);

            if (string.IsNullOrWhiteSpace(title))
            {
                SendForFailedTitle(data);
                return;
            }

            PublishOutputMessage(new TweetOutputData 
            {
                User = data.User,
                WasPage = true,
                OutText = data.Text,
                PageTitle = title
            });
        }

        /// <summary>
        /// If the title could not be received for some reason, send the tweet as a standard one
        /// </summary>
        private void SendForFailedTitle(TweetLinkedPageRequestData data) => 
            PublishOutputMessage(new TweetOutputData { User = data.User, WasPage = false, OutText = data.Text });

        /// <summary>
        /// Send the output message that contains the page title
        /// </summary>
        private void PublishOutputMessage(TweetOutputData data) => 
            _messagePublisher.SendMessage(new ProcessingMessage { Topic = PostProcessOutgoingTopic, Data = data.ToByteString() });

        /// <summary>
        /// Retrieve the page title
        /// </summary>
        /// <param name="target">The target URL</param>
        /// <returns>The title of the target page; or null if it could not be retrieved</returns>
        private async Task<string> RetrievePageTitle(string target)
        {
            using(HttpResponseMessage response = await _httpClient.GetAsync(target))
            using(HttpContent content = response.Content)
            {
                var strContent = await content.ReadAsStringAsync();
                var match = _titleRegex.Match(strContent);

                if (!match.Success || match.Groups.Count < 2)
                    return null;

                return match.Groups[1].Value;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_messageClient != null)
                        _messageClient.Dispose();

                    if (_messageSubscription != null)
                        _messageSubscription.Dispose();

                    if (_httpClient != null)
                        _httpClient.Dispose();

                    if (_httpClientHandler != null)
                        _httpClientHandler.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() => Dispose(true);

        #endregion
    }
}