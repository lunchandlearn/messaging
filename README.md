# Messaging Demo #

This repo contains a simple example of using a message queue (in this case ZeroMQ) to keep code separated and avoid 
coupling. 

## Overview ##
This app streams tweets in real-time from Twitter's live streaming [API](https://dev.twitter.com/streaming/overview).
When new tweets are received, they are processed on Observable streams and sent along the [NetMQ](https://github.com/zeromq/netmq) messaging
system for further processing. The process is illustrated below, with all communications being done through the [ZeroMQ](http://zeromq.org)
publisher and clients.

![Application Flow](Process.png)

Where tweets contianing "t.co" links are sent to the PageScraper to obtain the title attribute of the linked page prior to being
printed to the console. All other tweets are sent directly to the TweetPrinter to be output to the console.

## Getting Started ##
To run the project, there are a couple of steps to be completed.

1. Clone this repository
2. Download the RC4 release of .net core [HERE](https://github.com/dotnet/core/blob/master/release-notes/rc4-download.md) and install
3. Install visual studio code, available [HERE](http://code.visualstudio.com/)
4. Open your terminal, and navigate to the root of this repo. Then run `dotnet restore`

### Twitter Configuration ###
To run the code in this repo, you need to create and register an app with Twitter, which is free. 
You can log in with your Twitter account at apps.twitter.com and create a new app. The website URL can be simply 
set to https://www.google.com, and the callback URL is not important. Once the app has been created, go to the
tab labeled 'Keys and Access Tokens' and create your access token. You will need four values from this page:

1. Consumer Key
2. Consumer Secret
3. Access Token
4. Access Token Secret

Once you have these values, create a new file in the root folder of this project named `twitter_auth.json`. Inside of this
file, add the values in this format.

```
{
	"TwitterConfigurations": {
		"ConsumerKey": "YOUR KEY HERE",
		"ConsumerSecret": "YOUR SECRET HERE",
		"UserAccessToken": "ACCESS TOKEN HERE",
		"UserAccessSecret": "ACCESS TOKEN SECRET HERE"
	}
}
```

And with that you should be able to run the code in this repository.

## Running ##
Once you have done all steps in Getting Started, you can simply issue the `dotnet run` command to run the application.
If there are any problems, ensure you have run `dotnet restore` at least once before to obtain all the dependencies of
this project.

## Potential Issues ##
It has been noted that when running on macOS 10.12.4 beta, debugging & running .net core application in visual studio 
code is broken. While you can't debug and run from the editor, you can still run from the console. Windows installes
do not experience this issue. You can follow the progress on this issue [HERE](https://github.com/OmniSharp/omnisharp-vscode/issues/1220#issuecomment-280738130)