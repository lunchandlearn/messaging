using System;
using System.Linq;
using System.Reactive.Linq;
using Google.Protobuf;
using Pentland.LunchAndLearn.Messaging.Client;
using Pentland.LunchAndLearn.Messaging.Messages;
using Pentland.LunchAndLearn.Messaging.Publisher;
using Pentland.LunchAndLearn.Proto;

namespace Pentland.LunchAndLearn.Processing
{
    /// <summary>
    /// The TweetProcessor handles routing of incoming tweets and 
    /// either sends them to be printed or to have site details scraped
    /// </summary>
    public class TweetProcessor : IDisposable
    {
        private const MessageTopic TargetIncomingTopic = MessageTopic.Tweet;

        private readonly MessagePublisher _messagePublisher;

        private readonly MessageClient _messageClient;

        private IDisposable _messageSubscription;

        public TweetProcessor(MessagePublisher publisher)
        {
            if (publisher == null)
                throw new ArgumentNullException(nameof(publisher));

            _messagePublisher = publisher;
            _messageClient = new MessageClient(TargetIncomingTopic);
        }

        public void StartProcessingIncoming()
        {
            //LL3
            // Process incoming tweets and send them to routing function
            _messageSubscription = _messageClient.MessageObservable
                .Where(msg => msg != null)
                .Select(msg => TweetContentData.Parser.ParseFrom(msg.Data))
                .Subscribe(RouteIncomingMessages);
            _messageClient.RunMessageListener();
        }

        private void RouteIncomingMessages(TweetContentData data)
        {
            if (data.Text.Contains("https://t.co"))
                ContainsUrl(data);
            else
                StandardTweet(data);
        }

        private void ContainsUrl(TweetContentData data)
        {
            var pieces = data.Text.Split(' ');

            var link = pieces
                .Where(piece => piece.StartsWith("https://t.co"))
                .FirstOrDefault();

            //LL4
            // Process based on the linke we did or did not find
            if (string.IsNullOrWhiteSpace(link))
            {
                StandardTweet(data);
                return;
            }
            
            var messageContent = new TweetLinkedPageRequestData
            {
                User = data.User,
                Url = link,
                Text = data.Text
            };

            var message = new ProcessingMessage
            {
                Topic = MessageTopic.UrlRequest,
                Data = messageContent.ToByteString()
            };

            _messagePublisher.SendMessage(message);
        }

        private void StandardTweet(TweetContentData data)
        {
            //LL5
            // For any tweets that do not have t.co links, just convert and send them
            // back out to the publisher for someone else to process
            var content = new TweetOutputData
            {
                User = data.User,
                WasPage = false,
                OutText = data.Text
            };

            var message = new ProcessingMessage
            {
                Topic = MessageTopic.OutputData,
                Data = content.ToByteString()
            };

            _messagePublisher.SendMessage(message);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_messageClient != null)
                        _messageClient.Dispose();

                    if (_messageSubscription != null)
                        _messageSubscription.Dispose();

                    _messageSubscription = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() => Dispose(true);
        #endregion
    }
}