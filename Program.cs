﻿using Pentland.LunchAndLearn.Messaging.Broker;
using System.Threading;
using Pentland.LunchAndLearn.Configurations;
using Pentland.LunchAndLearn.Twitter;
using Pentland.LunchAndLearn.Messaging.Publisher;
using Pentland.LunchAndLearn.Output;
using Pentland.LunchAndLearn.Processing;
using System;
using Pentland.LunchAndLearn.Scraping;

namespace Pentland.LunchAndLearn
{
    public class MessagingDemo
    {
        /// <summary>
        /// The message broker that handles distributing messages from publishers
        /// to interested clients
        /// </summary>
        private readonly MessageBroker _messageBroker;

        /// <summary>
        /// The streamer that connects to Twitter and receives new tweets in real-time
        /// </summary>
        private readonly TwitterStreamer _twitterStreamer;

        /// <summary>
        /// The publisher that is used to send outgoing messages to the publisher
        /// </summary>
        private readonly MessagePublisher _publisher;

        /// <summary>
        /// The printer that is responsible for outputting tweets to the console
        /// </summary>
        private readonly TweetPrinter _twitterPrinter;
        
        /// <summary>
        /// The tweet processor
        /// </summary>
        private readonly TweetProcessor _tweetProcessor;

        /// <summary>
        /// Scraper for linked pages
        /// </summary>
        private readonly PageScraper _pageScraper;

        /// <summary>
        /// Create a new MessagingDemo
        /// </summary>
        public MessagingDemo()
        {
            BuildConfiguration();
            _publisher = new MessagePublisher();
            _messageBroker = new MessageBroker();       
            _twitterStreamer = new TwitterStreamer(_publisher);
            _twitterPrinter = new TweetPrinter();
            _pageScraper = new PageScraper(_publisher);
            _tweetProcessor = new TweetProcessor(_publisher);
        }

        /// <summary>
        /// Run the demo
        /// </summary>
        public void RunDemo()
        {
            RunBroker();
            RunPrinter();
            RunScraper();
            RunProcessor();
            RunTwitterStreamer();
        }

        /// <summary>
        /// Clean up the demo resources when everything is complete
        /// </summary>
        public void DemoComplete()
        {
            Console.WriteLine("Stopping... Cleaning up resources.");
            CleanupDemo();
        }

        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args">Command line args, not used</param>
        public static void Main(string[] args)
        {
            var demo = new MessagingDemo();
            demo.RunDemo();
            Thread.Sleep(TimeSpan.FromMinutes(2));
            demo.DemoComplete();
        }

        /// <summary>
        /// Build the configuration for the program and twitter
        /// </summary>
        private void BuildConfiguration() => Config.Init();

        /// <summary>
        /// Run the broker
        /// </summary>
        private void RunBroker() => _messageBroker.RunBroker();

        /// <summary>
        /// Stop the broker
        /// </summary>
        private void StopBroker() => _messageBroker.StopBroker();

        /// <summary>
        /// Run the twitter streamer
        /// </summary>        
        private void RunTwitterStreamer() => _twitterStreamer.RunTwitterStream();

        /// <summary>
        /// Stop the twitter streamer
        /// </summary>
        private void StopTwitterStreamer() => _twitterStreamer.Dispose();

        /// <summary>
        /// Run the output printer
        /// </summary>
        private void RunPrinter() => _twitterPrinter.StartProcessingIncomingTweets();

        /// <summary>
        /// Stop the output printer
        /// </summary>
        private void StopPrinter() => _twitterPrinter.Dispose();

        /// <summary>
        /// Run the site scraper
        /// </summary>
        private void RunScraper() => _pageScraper.BeginProcessingPageRequests();

        /// <summary>
        /// Stop the site scraper
        /// </summary>
        private void StopScraper() => _pageScraper.Dispose();

        /// <summary>
        /// Run the tweet processor
        /// </summary>
        private void RunProcessor() => _tweetProcessor.StartProcessingIncoming();

        /// <summary>
        /// Stop the tweet processor
        /// </summary>
        private void StopProcessor() => _tweetProcessor.Dispose();

        /// <summary>
        /// Clean up demo resources
        /// </summary>
        private void CleanupDemo()
        {
            StopTwitterStreamer();
            StopProcessor();
            StopScraper();
            StopPrinter();
            StopBroker();
            Console.WriteLine("Stopping... Cleanup complete");
        }
    }
}
