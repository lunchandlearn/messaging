namespace Pentland.LunchAndLearn.Configurations
{
    /// <summary>
    /// This is the configuration that contains authentication information
    /// for Twitter. It is populated from the twitter_auth.json file
    /// </summary>
    internal class TwitterConfiguration
    {
        /// <summary>
        /// The Twitter consumer key
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// The Twitter consumer secret
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// The user access token
        /// </summary>
        public string UserAccessToken { get; set; }

        /// <summary>
        /// The user access secret
        /// </summary>
        public string UserAccessSecret { get; set; }
    }
}