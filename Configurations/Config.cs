using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Pentland.LunchAndLearn.Configurations
{
    /// <summary>
    /// Configuration class containing populated instances
    /// of App and Twitter configurations.
    /// </summary>
    internal static class Config
    {
        /// <summary>
        /// List of terms for Twitter
        /// </summary>
        private static List<string> _termList;

        /// <summary>
        /// The terms we want to scan the Twitter stream for
        /// </summary>
        public static List<string> TwitterTrackingTerms => _termList;

        /// <summary>
        /// Tweet language filter
        /// </summary>
        public static string TwitterStreamLanguageFilter => "en";

        /// <summary>
        /// The app level configuration read from config.json
        /// </summary>
        public static AppConfiguration AppConfig { get; private set; }

        /// <summary>
        /// The twitter authentication configuration, should be read from twitter_auth.json
        /// </summary>
        public static TwitterConfiguration TwitterConfig { get; private set; }

        /// <summary>
        /// Initialize the config
        /// </summary>
        public static void Init()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("Config.json", false)
                .AddJsonFile("twitter_auth.json", false)
                .Build();

            AppConfig = new AppConfiguration();
            TwitterConfig = new TwitterConfiguration();

            config.GetSection("AppConfiguration").Bind(AppConfig);
            config.GetSection("TwitterConfiguration").Bind(TwitterConfig);
            _termList = new List<string>
            {
                "git",
                "github",
                "golang",
                "dotnet",
                "python",
                "C#"
            };
        }
    }
}