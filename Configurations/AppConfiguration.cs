namespace Pentland.LunchAndLearn.Configurations
{
    /// <summary>
    /// Standard configuration options for the app
    /// </summary>
    internal class AppConfiguration 
    {
        /// <summary>
        /// The port that the broker uses for its publisher socket.
        /// Clients should connect to this port
        /// </summary>
        public int PublisherPort { get; set; }

        /// <summary>
        /// The port that the broker uses for its subscriber socket.
        /// Publishers should connect to this port
        /// </summary>
        public int SubscriberPort { get; set; }

        /// <summary>
        /// The address all clients/publishers connect to
        /// </summary>
        public string ConnectAddress { get; set; }
    }
}