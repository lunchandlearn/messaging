using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Pentland.LunchAndLearn.Configurations;
using Pentland.LunchAndLearn.Messaging.Messages;
using Pentland.LunchAndLearn.Messaging.Publisher;
using Pentland.LunchAndLearn.Proto;
using Tweetinvi;
using Tweetinvi.Events;
using Tweetinvi.Models;
using Tweetinvi.Streaming;
using Google.Protobuf;

namespace Pentland.LunchAndLearn.Twitter
{
    public class TwitterStreamer : IDisposable
    {
        /// <summary>
        /// Our reference to the message publisher
        /// </summary>
        private readonly MessagePublisher _messagePublisher;

        /// <summary>
        /// The stream we are using to connect to Twitter with
        /// </summary>
        private IFilteredStream _twitterStream;

        /// <summary>
        /// The Subject that all incoming tweet data is sent along
        /// </summary>
        private Subject<MatchedTweetReceivedEventArgs> _tweetSubject;

        /// <summary>
        /// Create a new instance of <see cref="TwitterStreamer">
        /// </summary>
        /// <param name="publisher">The message publisher that new tweets are sent out on</param>
        public TwitterStreamer(MessagePublisher publisher)
        {
            if (publisher == null)
                throw new ArgumentNullException(nameof(publisher));

            _messagePublisher = publisher;
            SetTwitterAuthFromConfig();
        }

        /// <summary>
        /// Run the twitter stream
        /// </summary>
        public void RunTwitterStream()
        {
            StreamFromTwitter();
            //LL2
            // Once the stream is started, filter and process
            _tweetSubject
                .Where(args => args != null && args.Tweet != null)
                .Select(args => args.Tweet)
                .Subscribe(PublishMessage,
                    err => Console.WriteLine($"Error in stream!\n Details: {err}"),
                    () => { });
        }

        /// <summary>
        /// Publish a new tweet using the message publisher
        /// </summary>
        /// <param name="outputTweet">The outgoing tweet data</param>
        private void PublishMessage(ITweet outputTweet)
        {
            //LL1
            // When we have a message to work with, send it to the publisher
            var messageContent = new TweetContentData
            {
                User = outputTweet.CreatedBy.Name,
                Text = outputTweet.FullText
            };

            var message = new ProcessingMessage
            {
                Topic = MessageTopic.Tweet,
                Data = messageContent.ToByteString()
            };

            _messagePublisher.SendMessage(message);
        }

        /// <summary>
        /// Start streaming from twitter
        /// </summary>
        private void StreamFromTwitter()
        {
            CreateConfiguredStream();
            ConfigureStreamEventHandlers();

            _twitterStream.StartStreamMatchingAnyConditionAsync();
        }

        /// <summary>
        /// Configure TweetInvi authentication
        /// </summary>
        private void SetTwitterAuthFromConfig()
        {
            var config = Config.TwitterConfig;

            Auth.SetUserCredentials(config.ConsumerKey, config.ConsumerSecret, config.UserAccessToken, config.UserAccessSecret);
        }

        /// <summary>
        /// Create a new configured stream
        /// </summary>
        private void CreateConfiguredStream()
        {
            _twitterStream = Stream.CreateFilteredStream();
            Config.TwitterTrackingTerms.ForEach(track => _twitterStream.AddTrack(track));
            _twitterStream.AddTweetLanguageFilter(Config.TwitterStreamLanguageFilter);
            _twitterStream.StallWarnings = true;
        }

        /// <summary>
        /// Configure the event handlers for the twitter stream
        /// </summary>
        private void ConfigureStreamEventHandlers()
        {
            if (_twitterStream == null)
                return;

            _tweetSubject = new Subject<MatchedTweetReceivedEventArgs>();

            _twitterStream.MatchingTweetReceived += HandleMatchingTweet;

            _twitterStream.DisconnectMessageReceived += HandleDisconnectMessage;

            _twitterStream.WarningFallingBehindDetected += HandleFallingBehindDetectedWarning;
        }

        /// <summary>
        /// Handle an incoming tweet
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="args">The event args</param>
        private void HandleMatchingTweet(object sender, MatchedTweetReceivedEventArgs args) => _tweetSubject.OnNext(args);

        /// <summary>
        /// Handle an incoming disconnect message
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="args">The event args</param>
        private void HandleDisconnectMessage(object sender, DisconnectedEventArgs args)
        {
            _twitterStream.StopStream();
            _tweetSubject.OnCompleted();
        }

        /// <summary>
        /// Handle incoming falling behind alerts
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="args">The event args</param>
        private void HandleFallingBehindDetectedWarning(object sender, WarningFallingBehindEventArgs args)
        {
            if (args.WarningMessage.PercentFull > 80)
            {
                Console.WriteLine($"STALL WARNING: {args.WarningMessage}\n AT OR ABOVE THRESHOLD, STOPPING STREAM");
                _twitterStream.StopStream();
                _tweetSubject.OnCompleted();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_twitterStream != null)
                        _twitterStream.StopStream();

                    _twitterStream = null;

                    if (_tweetSubject != null)
                    {
                        _tweetSubject.OnCompleted();
                        _tweetSubject.Dispose();
                        _tweetSubject = null;
                    }
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}