using System;
using System.Reactive.Linq;
using Pentland.LunchAndLearn.Messaging.Client;
using Pentland.LunchAndLearn.Messaging.Messages;
using Pentland.LunchAndLearn.Proto;

namespace Pentland.LunchAndLearn.Output
{
    /// <summary>
    /// The TweetPrinter handles printing the processed tweets to the 
    /// console
    /// </summary>
    public class TweetPrinter : IDisposable
    {
        /// <summary>
        /// The desired message topic
        /// </summary>
        private const MessageTopic DesiredTopic = MessageTopic.OutputData;

        /// <summary>
        /// Template string for standard tweets
        /// </summary>
        private const string StdTweetFormat = "\nUser {0} just tweeted: '{1}'";

        /// <summary>
        /// Template string for webpage tweets
        /// </summary>
        private const string WebTweetFormat = "\nUser {0} just tweeted: '{1}'\n{0} also linked a page with title '{2}'";
        
        /// <summary>
        /// The message client that delivers incoming tweet messages
        /// </summary>
        private readonly MessageClient _messageClient;

        /// <summary>
        /// The reference to the message subscription
        /// </summary>
        private IDisposable _messageSubscription;

        /// <summary>
        /// Create a new <see cref="TweetPrinter"/>
        /// </summary>
        public TweetPrinter()
        {
            _messageClient = new MessageClient(DesiredTopic);
        }

        /// <summary>
        /// Begin processing the incoming tweet output messages
        /// </summary>
        public void StartProcessingIncomingTweets()
        {
            //LL9
            // Begin listening for incoming messages, deserialize the contents, and
            // process them
            _messageSubscription = _messageClient.MessageObservable
                .Where(msg => msg != null)
                .Select(msg => TweetOutputData.Parser.ParseFrom(msg.Data))
                .Subscribe(PrintTweetMessage);
            _messageClient.RunMessageListener();
        }

        /// <summary>
        /// Send the incoming data to the proper output printer
        /// </summary>
        /// <param name="data">The incoming data</param>
        private void PrintTweetMessage(TweetOutputData data)
        {
            //LL10
            // Process the incoming messages according to whether they include a web page
            // title or not
            if (data.WasPage)
                PrintPageTweet(data);                
            else
                PrintNormalTweet(data);
        }

        /// <summary>
        /// Print a tweet that contained a link
        /// </summary>
        private void PrintPageTweet(TweetOutputData data) => Console.WriteLine(string.Format(WebTweetFormat, data.User, data.OutText, data.PageTitle));

        /// <summary>
        /// Print a normal tweet
        /// </summary>
        private void PrintNormalTweet(TweetOutputData data) => Console.WriteLine(string.Format(StdTweetFormat, data.User, data.OutText));

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_messageSubscription != null)
                        _messageSubscription.Dispose();
                    
                    if (_messageClient != null)
                        _messageClient.Dispose();

                    _messageSubscription = null;
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() => Dispose(true);
        #endregion
    }
}