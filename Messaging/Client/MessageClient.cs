using System;
using System.Reactive.Subjects;
using Google.Protobuf;
using NetMQ;
using NetMQ.Sockets;
using Pentland.LunchAndLearn.Configurations;
using Pentland.LunchAndLearn.Messaging.Messages;

namespace Pentland.LunchAndLearn.Messaging.Client
{
    /// <summary>
    /// The client that connects to the broker
    /// </summary>
    internal class MessageClient : IDisposable
    {
        /// <summary>
        /// The subject that incoming messages will be sent back out on
        /// </summary>
        private readonly Subject<ProcessingMessage> _messageSubject;

        /// <summary>
        /// The topic that will be used for subscription
        /// </summary>
        private readonly MessageTopic _targetTopic;

        /// <summary>
        /// The subscriber socket that connects to the broker
        /// </summary>
        private SubscriberSocket _subSocket;

        /// <summary>
        /// The poller used to monitor the socket
        /// </summary>
        private NetMQPoller _poller;

        /// <summary>
        /// The observable sequence that the user of this client will 
        /// subscribe to in order to receive messages
        /// </summary>
        public IObservable<ProcessingMessage> MessageObservable { get; }

        /// <summary>
        /// Create a new MessageClient
        /// </summary>
        /// <param name="topic">The target topic</param>
        public MessageClient(MessageTopic topic)
        {
            _messageSubject = new Subject<ProcessingMessage>();
            MessageObservable = _messageSubject;
            _targetTopic = topic;
        }

        /// <summary>
        /// Run the listener to receive incoming messages
        /// </summary>
        public void RunMessageListener()
        {
            _subSocket = new SubscriberSocket($">tcp://{Config.AppConfig.ConnectAddress}:{Config.AppConfig.PublisherPort}");

            _poller = new NetMQPoller { _subSocket };
            _subSocket.Subscribe(_targetTopic.ToTopicString());

            _subSocket.ReceiveReady += HandleIncomingMessages;
            
            _poller.RunAsync();
        }

        /// <summary>
        /// Handler for incoming NetMQ events
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="args">The event args containing the message</param>
        private void HandleIncomingMessages(object sender, NetMQSocketEventArgs args)
        {
            var incoming = args.Socket.ReceiveMultipartMessage();
            var message = new ProcessingMessage
            {
                Topic = incoming.First.ConvertToString().ToTopic(),
                Data = ByteString.CopyFrom(incoming.Last.ToByteArray())
            };

            _messageSubject.OnNext(message);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_poller != null)
                    {
                        if (_poller.IsRunning)
                            _poller.Stop();

                        _poller.Dispose();
                        _poller = null;
                    }

                    if (_subSocket != null)
                    {
                        _subSocket.Disconnect($"tcp://{Config.AppConfig.ConnectAddress}:{Config.AppConfig.PublisherPort}");
                        _subSocket.Close();
                        _subSocket.Dispose();
                        _subSocket = null;
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() => Dispose(true);
        #endregion
    }
}