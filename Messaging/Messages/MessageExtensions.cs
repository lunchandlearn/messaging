using System.Collections.Generic;
using NetMQ;

namespace Pentland.LunchAndLearn.Messaging.Messages
{
    public static class MessageExtensions
    {
        public static NetMQMessage ToNetMQMessage(this ProcessingMessage message)
        {
            var frames = new List<NetMQFrame>
            {
                new NetMQFrame(message.Topic.ToTopicString()),
                new NetMQFrame(message.Data.ToByteArray())                
            };

            return new NetMQMessage(frames);
        }
    }
}