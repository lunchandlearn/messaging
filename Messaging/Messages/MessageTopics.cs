using System;
using System.Collections.Generic;
using System.Linq;

namespace Pentland.LunchAndLearn.Messaging.Messages
{
    public enum MessageTopic
    {
        Tweet,
        UrlRequest,
        OutputData
    }

    internal static class MessageTopicConverters
    {
        private static readonly Dictionary<MessageTopic, string> TopicMappings = new Dictionary<MessageTopic, string>
        {
            { MessageTopic.Tweet, "TWEET" },
            { MessageTopic.OutputData, "OUTPUT" },
            { MessageTopic.UrlRequest, "URLREQUEST" }
        };

        public static string ToTopicString(this MessageTopic topic) => TopicMappings[topic];

        public static MessageTopic ToTopic(this string topic)
        {
            if (!TopicMappings.ContainsValue(topic))
                throw new ArgumentException("Invalid topic");

            return TopicMappings.FirstOrDefault(mapping => mapping.Value.Equals(topic, StringComparison.OrdinalIgnoreCase)).Key;
        }
    }
}