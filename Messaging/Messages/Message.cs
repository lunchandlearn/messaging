using Google.Protobuf;

namespace Pentland.LunchAndLearn.Messaging.Messages
{
    public class ProcessingMessage 
    {
        public MessageTopic Topic { get; set; }
        public ByteString Data { get; set; }
    }
}