using System;
using System.Reactive.Subjects;
using NetMQ;
using NetMQ.Sockets;
using Pentland.LunchAndLearn.Configurations;
using Pentland.LunchAndLearn.Messaging.Messages;

namespace Pentland.LunchAndLearn.Messaging.Publisher
{
    /// <summary>
    /// The MessagePublisher provides message publishing functionality
    /// </summary>
    public class MessagePublisher : IDisposable
    {
        /// <summary>
        /// Our monostate MessageAgent, used to send all messages to the broker
        /// </summary>
        private static readonly Lazy<MessageAgent> _agent = new Lazy<MessageAgent>(() => new MessageAgent(), false);

        /// <summary>
        /// Send the given message off to the publisher
        /// </summary>
        /// <param name="msg">The message</param>
        public void SendMessage(ProcessingMessage msg) => SendOutgoingMessage(msg);

        /// <summary>
        /// Enque the message with the agent
        /// </summary>
        /// <param name="msg">The message</param>
        private void SendOutgoingMessage(ProcessingMessage msg) => _agent.Value.EnqueueMessage(msg);

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _agent.Value.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose() => Dispose(true);
        #endregion

        /// <summary>
        /// The MessageAgent is responsible for transforming user messages and sending them
        /// to the broker
        /// </summary>
        private class MessageAgent : IDisposable
        {
            /// <summary>
            /// The socket for outgoing messages
            /// </summary>
            private PublisherSocket _publisherSocket;

            /// <summary>
            /// The subject messages are sent out over
            /// </summary>
            private readonly Subject<ProcessingMessage> _incomingMessageSubject;

            /// <summary>
            /// Create a new MessageAgent
            /// </summary>
            public MessageAgent()
            {
                BuildPublisherSocket();
                _incomingMessageSubject = new Subject<ProcessingMessage>();
                _incomingMessageSubject.Subscribe(SendMessage);
            }

            /// <summary>
            /// Enqueue an incoming ProcessingMessage
            /// </summary>
            /// <param name="msg"></param>
            public void EnqueueMessage(ProcessingMessage msg) => _incomingMessageSubject.OnNext(msg);

            /// <summary>
            /// Build the publisher socket
            /// </summary>
            private void BuildPublisherSocket()
            {
                _publisherSocket = new PublisherSocket();
                _publisherSocket.Connect($"tcp://{Config.AppConfig.ConnectAddress}:{Config.AppConfig.SubscriberPort}");
            }

            /// <summary>
            /// Transform and send the given message out over the publisher socket
            /// </summary>
            /// <param name="message">The message</param>
            public void SendMessage(ProcessingMessage message)
            {
                var msg = message.ToNetMQMessage();
                _publisherSocket.SendMultipartMessage(msg);
            }

            private bool disposedValue = false;

            public void Dispose() => Dispose(true);

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        if (_publisherSocket != null)
                        {
                            _publisherSocket.Disconnect($"tcp://{Config.AppConfig.ConnectAddress}:{Config.AppConfig.SubscriberPort}");
                            _publisherSocket.Close();
                            _publisherSocket.Dispose();
                            _publisherSocket = null;
                        }                        
                    }
                    disposedValue = true;
                }
            }
        }
    }
}