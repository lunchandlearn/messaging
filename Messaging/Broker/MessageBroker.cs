using System;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;
using Pentland.LunchAndLearn.Configurations;

namespace Pentland.LunchAndLearn.Messaging.Broker
{
    /// <summary>
    /// This is the message broker that handles connecting the various
    /// publishers and clients
    /// </summary>
    internal class MessageBroker
    {
        /// <summary>
        /// The actual NetMQ proxy
        /// </summary>
        private Proxy _proxy;

        /// <summary>
        /// Run the broker
        /// </summary>
        public void RunBroker()
        {
            var brokerThread = new Thread(StartBroker) { IsBackground = true };
            brokerThread.Start();
        }

        /// <summary>
        /// Stop the broker
        /// </summary>
        public void StopBroker()
        {
            if (_proxy != null)
                _proxy.Stop();
        }

        /// <summary>
        /// Method that actually handles creating the sockets and running the broker
        /// itself.
        /// </summary>
        private void StartBroker()
        {
            var configuration = Config.AppConfig;
            using (var xPubSocket = new XPublisherSocket($"@tcp://{configuration.ConnectAddress}:{configuration.PublisherPort}"))
            using (var xSubSocket = new XSubscriberSocket($"@tcp://{configuration.ConnectAddress}:{configuration.SubscriberPort}"))
            {
                Console.WriteLine("Starting the intermediary");
                _proxy = new Proxy(xSubSocket, xPubSocket);

                _proxy.Start();
            }
        }
    }
}